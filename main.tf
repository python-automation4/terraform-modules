provider "aws" {
  region = "ap-south-1"
}

module "s3" {
    source                            = "./modules/s3_with_lambda"
    s3_bucket_name                    = var.s3_bucket_name
    s3_bucket_authfunc_key            = var.s3_bucket_authfunc_key
    s3_bucket_policy_key              = var.s3_bucket_policy_key
    s3_authfunction_local_key_path    = var.s3_authfunction_local_key_path
    s3_policyfile_local_key_path      = var.s3_policyfile_local_key_path
    lambda_function_name              = var.lambda_function_name
    lambda-run_time                   = var.lambda-run_time
    okta_audience                     = var.okta_audience
    okta_issuer                       = var.okta_issuer
    okta_client_id                    = var.okta_client_id
}

module "apigateway" {
    source                            = "./modules/apigateway"
    region                            = var.region
    vpc_id                            = var.vpc_id  
    alb_name                          = var.alb_name
    nlb_name                          = var.nlb_name
    nlb_tg_name                       = var.nlb_tg_name
    api_name                          = var.api_name
    resource_path                     = var.resource_path
    vpc_link_name                     = var.vpc_link_name
    authorization_header_value        = var.authorization_header_value
    lambda_authorizer_name            = var.lambda_authorizer_name
    lambda_function_arn               = module.s3.lambda_function_arn #var.lambda_function_arn
    deploy_stage_name                 = var.deploy_stage_name   
    script_file_path                  = var.script_file_path
    certificate_domain                = var.certificate_domain
    custom_domain_name                = var.custom_domain_name
    hosted_zone_name                  = var.hosted_zone_name
    depends_on                        = [module.s3]    
}

module "WAF" {
    source                            = "./modules/waf"
    ip_set_name                       = var.ip_set_name
    ip_list                           = var.ip_list
    web_acl_name                      = var.web_acl_name
    api_gateway_stage_arn             = module.apigateway.rest_api_stage_arn 
    ip_rules                          = var.ip_rules
    managed_rules                     = var.managed_rules
    depends_on                        = [module.apigateway]
}

module "cloudwatch" {
    source                            = "./modules/cloudwatch"
    dashboard_name                    = var.dashboard_name
    stage                             = module.apigateway.api_deploy_stage_name
    rest_api_name                     = module.apigateway.rest_api_name
    region                            = var.region
    depends_on                        = [module.WAF]
}