#s3_with_lambda
variable "s3_bucket_name" {
}

variable "s3_bucket_authfunc_key" {
}

variable "s3_bucket_policy_key" {
}

variable "s3_authfunction_local_key_path" {
}

variable "s3_policyfile_local_key_path" {
}

variable "lambda_function_name" {
}

variable "lambda-run_time" {
}

variable "okta_audience" {
}

variable "okta_issuer" {
}

variable "okta_client_id" {
}

#api_gateway
variable "region" {
}

variable "vpc_link_name" {
}

variable "vpc_id" {
}

variable "alb_name" {
}

variable "nlb_name" {
}

variable "nlb_tg_name" {
}

variable "api_name" {
}

variable "resource_path" {
}

variable "authorization_header_value" {
}

variable "lambda_authorizer_name" {
}

variable "deploy_stage_name" {
}

variable "script_file_path" {
}

variable "certificate_domain" {  
}

variable "custom_domain_name"{
}

variable "hosted_zone_name" {
}

# variable "lambda_function_arn" {
# }

#waf
variable "ip_set_name" {
}

variable "ip_list" {
}

variable "web_acl_name" {
}

variable "ip_rules" {
}

variable "managed_rules" {
}

#variable "api_gateway_stage_arn" {
#}

#cloudwatch
variable "dashboard_name" {
}
    
