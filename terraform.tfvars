# S3 related vars
s3_bucket_name = "test-api-bucket-109837"
s3_bucket_authfunc_key = "lambda_auth_code_for_apigateway.zip"
s3_bucket_policy_key = "authorization_policy_file.json"
s3_authfunction_local_key_path = "lambda_auth_code_for_apigateway.zip"
s3_policyfile_local_key_path = "authorization_policy_file.json"
lambda_function_name = "test_lambda_gateway"
lambda-run_time = "nodejs18.x"
okta_audience = "api://apigateway"
okta_issuer = "https://moderna-portal.oktapreview.com"
okta_client_id = "123456789"

# api_gateway related vars
region = "ap-south-1"
vpc_id = "vpc-0263134527f3ac0cb"
alb_name = "Private-ALB-2"
nlb_name = "API-Private-NLB-2"
nlb_tg_name = "nlb-target-group-2"
api_name = "test_rest_api"
vpc_link_name = "test-vpc-link"
resource_path = "admin"
authorization_header_value = "Basic 'abcdefghijkl'"
lambda_authorizer_name = "test_lambda_authorizer"
deploy_stage_name = "dev"
script_file_path = "modules/apigateway/scripts/api_stage_arn.py"
custom_domain_name = "testapi.raghavshetty.online"
certificate_domain = "test.raghavshetty.online" 
hosted_zone_name = "raghavshetty.online"

#waf vars
ip_set_name = "Blacklist_IP_Set-2"
ip_list = ["182.74.52.154/32"]
web_acl_name = "Test_APIGateway_WAF"
#api_gateway_stage_arn = ""
ip_rules = [
        {
            name = "Ip_Blocking"
            priority = 1
            action = "block"
            statement = "ip_set_reference_statement"
        },
        {
            name = "RateLoginLimit"
            priority = 2 
            action = "block"
               statement = "rate_based_statement"
        }
]
managed_rules = [
        {
            name = "AWSManagedRulesAmazonIpReputationList"
            priority = 3
            override_action = "count"
            vendor_name = "AWS"
        },
        {
            name = "AWSManagedRulesAnonymousIpList"
            priority = 4
            override_action = "count"
            vendor_name = "AWS"
        },
        {
            name = "AWSManagedRulesSQLiRuleSet"
            priority = 5
            override_action = "count"
            vendor_name = "AWS"
        }   
]

#cloudwatch vars
dashboard_name = "test-custom-dashboard"
