data "aws_subnet_ids" "private_subnets" {
  vpc_id            = var.vpc_id
  filter {
    name   = "tag:Type"
    values = ["Private"]
  }
}

# Get ALB ARN
data "aws_lb" "alb" {
  name = var.alb_name
}

# Create NLB
resource "aws_lb" "nlb" {
  name               = "test-API-Private-NLB"
  internal           = true
  load_balancer_type = "network"
  subnets            = data.aws_subnet_ids.private_subnets.ids
}

# Create NLB Target Group
resource "aws_lb_target_group" "nlb_target_group" {
  name              = "test-nlb-target-group"
  target_type       = "alb"
  port              = 80
  protocol          = "TCP"
  vpc_id            = var.vpc_id
  health_check {
    port     = 80
    protocol = "HTTP"
  }
}

# Create NLB Listener
resource "aws_lb_listener" "nlb_listener" {
  load_balancer_arn = aws_lb.nlb.arn
  port              = 80
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nlb_target_group.arn
  }
}

resource "aws_lb_target_group_attachment" "internal_alb_attachment" {
  target_group_arn = aws_lb_target_group.nlb_target_group.arn
  target_id        = data.aws_lb.alb.arn
  port             = 80
}

resource "aws_api_gateway_rest_api" "rest_api" {
  name              = var.api_name
  description       = "Proxy to handle requests to our API"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "api_resource" {
  parent_id   = aws_api_gateway_rest_api.rest_api.root_resource_id #here it is '/'
  path_part   = var.resource_path # here it is resource name
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
}

resource "aws_api_gateway_vpc_link" "nlb_vpc_link" {
  name        = var.vpc_link_name 
  description = "linking private nlb to API Gateway"
#   target_arns = [var.vpc_target_arn]
  target_arns = [aws_lb.nlb.arn]
}

# data "aws_lambda_function" "lambda" {
#   function_name = var.lambda_function
# }

resource "aws_api_gateway_authorizer" "lambda_authorizer" {
  rest_api_id        = aws_api_gateway_rest_api.rest_api.id
  name               = var.lambda_authorizer_name
  type               = "TOKEN"
  identity_source    = "method.request.header.Authorization"
  authorizer_uri     = var.lambda_function_arn
  authorizer_result_ttl_in_seconds = 300
}

resource "aws_api_gateway_method" "api_resource_method" {
  rest_api_id   = aws_api_gateway_rest_api.rest_api.id
  resource_id   = aws_api_gateway_resource.api_resource.id
  http_method   = "GET"
  authorization = "CUSTOM" #To set the above created authorizer
  authorizer_id = aws_api_gateway_authorizer.lambda_authorizer.id
}

resource "aws_api_gateway_integration" "method_integration" {
  rest_api_id          = aws_api_gateway_rest_api.rest_api.id
  resource_id          = aws_api_gateway_resource.api_resource.id
  http_method          = aws_api_gateway_method.api_resource_method.http_method
  integration_http_method = "GET"
  type                 = "HTTP_PROXY"
  uri                  = "http://${aws_lb.nlb.dns_name}" # Replace with your backend URL
  connection_type      = "VPC_LINK"
  connection_id        = aws_api_gateway_vpc_link.nlb_vpc_link.id
  request_parameters = {
    "integration.request.header.Authorization" = "'${var.authorization_header_value}'"
  }
}

resource "aws_api_gateway_deployment" "rest_api_deployment" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  stage_name  = var.deploy_stage_name
}

# resource "aws_api_gateway_domain_name" "custom_domain" {
#   domain_name              = var.custom_domain_name
#   certificate_arn          = var.custom_domain_acm_certificate_arn
#   security_policy          = "TLS_1_2"
#   endpoint_configuration {
#     types = ["REGIONAL"]
#   }
# }

# resource "aws_api_gateway_base_path_mapping" "custom_domain_map_api" {
#   rest_api_id = aws_api_gateway_rest_api.rest_api.id
#   stage_name  = var.deploy_stage_name
#   domain_name = aws_api_gateway_domain_name.custom_domain.domain_name
# }

