variable "vpc_link_name" {
}

variable "vpc_id" {
}

variable "alb_name" {
}

variable "api_name" {
}

variable "resource_path" {
}

variable "authorization_header_value" {
}

variable "lambda_authorizer_name" {
}

variable "lambda_function_arn" {
}

variable "deploy_stage_name" {
}

# variable "custom_domain_name"{
#     default = ""
# }

# variable "custom_domain_acm_certificate_arn"{
#     default = ""
# }
