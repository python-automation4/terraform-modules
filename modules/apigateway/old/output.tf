output "rest_api_name" {
    value = aws_api_gateway_rest_api.rest_api.name
}

output "api_deploy_stage_name" {
    value = aws_api_gateway_deployment.rest_api_deployment.stage_name
}
