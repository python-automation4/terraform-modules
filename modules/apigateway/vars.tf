variable "region" {
}

variable "vpc_id" {
}

variable "alb_name" {
}

variable "nlb_name" {
}

variable "nlb_tg_name" {
}

variable "api_name" {
}

variable "resource_path" {
}

variable "authorization_header_value" {
}

variable "lambda_authorizer_name" {
}

variable "lambda_function_arn" {
}

variable "deploy_stage_name" {
}

variable "script_file_path" {
}

variable "vpc_link_name" {
}

variable "certificate_domain" {  
}

variable "hosted_zone_name" {
}

variable "custom_domain_name"{
}

# variable "custom_domain_acm_certificate_arn"{
#     default = ""
# }