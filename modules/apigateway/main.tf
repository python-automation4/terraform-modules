data "aws_subnet_ids" "private_subnets" {
  vpc_id            = var.vpc_id
  filter {
    name   = "tag:Type"
    values = ["Private"]
  }
}

data "aws_lb" "alb" {
  name = var.alb_name
}

resource "aws_lb" "nlb" {
  name               = var.nlb_name
  internal           = true
  load_balancer_type = "network"
  subnets            = data.aws_subnet_ids.private_subnets.ids
  depends_on = [
    data.aws_subnet_ids.private_subnets
  ]
}

resource "aws_lb_target_group" "nlb_target_group" {
  name              = var.nlb_tg_name
  target_type       = "alb"
  port              = 80
  protocol          = "TCP"
  vpc_id            = var.vpc_id
  health_check {
    port     = 80
    protocol = "HTTP"
  }
  depends_on = [
    aws_lb.nlb
  ]
}

resource "aws_lb_listener" "nlb_listener" {
  load_balancer_arn = aws_lb.nlb.arn
  port              = 80
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nlb_target_group.arn
  }
  depends_on = [
    aws_lb_target_group.nlb_target_group
  ]
}

resource "aws_lb_target_group_attachment" "internal_alb_attachment" {
  target_group_arn = aws_lb_target_group.nlb_target_group.arn
  target_id        = data.aws_lb.alb.arn
  port             = 80
  depends_on = [
    aws_lb_listener.nlb_listener
  ]
}

resource "aws_api_gateway_vpc_link" "nlb_vpc_link" {
  name        = var.vpc_link_name
  description = "linking private nlb to API Gateway"
  target_arns = [aws_lb.nlb.arn]
  depends_on = [
    aws_lb_target_group_attachment.internal_alb_attachment
  ]
}

resource "aws_api_gateway_rest_api" "rest_api" {
  name              = var.api_name
  description       = "Proxy to handle requests to our API"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
  depends_on = [
    aws_api_gateway_vpc_link.nlb_vpc_link
  ]
}

resource "aws_api_gateway_resource" "api_resource" {
  parent_id   = aws_api_gateway_rest_api.rest_api.root_resource_id 
  path_part   = var.resource_path 
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  depends_on = [
    aws_api_gateway_rest_api.rest_api
  ]
}

resource "aws_api_gateway_authorizer" "lambda_authorizer" {
  rest_api_id        = aws_api_gateway_rest_api.rest_api.id
  name               = var.lambda_authorizer_name
  type               = "TOKEN"
  identity_source    = "method.request.header.Authorization"
  authorizer_uri     = var.lambda_function_arn  #Here lamba arn is removed as it taking as a module output from s3 in the parent main.tf
  authorizer_result_ttl_in_seconds = 300
  depends_on = [
    aws_api_gateway_resource.api_resource
  ]
}

resource "aws_api_gateway_method" "api_resource_method" {
  rest_api_id   = aws_api_gateway_rest_api.rest_api.id
  resource_id   = aws_api_gateway_resource.api_resource.id
  http_method   = "GET"
  authorization = "CUSTOM" #To set the above created authorizer
  authorizer_id = aws_api_gateway_authorizer.lambda_authorizer.id
  depends_on = [
    aws_api_gateway_authorizer.lambda_authorizer
  ]
}

resource "aws_api_gateway_integration" "method_integration" {
  rest_api_id          = aws_api_gateway_rest_api.rest_api.id
  resource_id          = aws_api_gateway_resource.api_resource.id
  http_method          = aws_api_gateway_method.api_resource_method.http_method
  integration_http_method = "GET"
  type                 = "HTTP_PROXY"
  uri                  = "http://${aws_lb.nlb.dns_name}" # Replace with your backend URL
  connection_type      = "VPC_LINK"
  connection_id        = aws_api_gateway_vpc_link.nlb_vpc_link.id
  request_parameters = {
    "integration.request.header.Authorization" = "'${var.authorization_header_value}'"
  }
  depends_on = [
    aws_api_gateway_method.api_resource_method
  ]
}

resource "aws_api_gateway_deployment" "rest_api_deployment" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  stage_name  = var.deploy_stage_name
  depends_on = [
    aws_api_gateway_integration.method_integration
  ]
}

data "external" "get_rest_api_stage_arn" {
  program    = ["python3", var.script_file_path, var.region, var.api_name]
  depends_on = [
    aws_api_gateway_deployment.rest_api_deployment
  ]
}

data "aws_acm_certificate" "my_certificate" {
  domain   = var.certificate_domain
  statuses = ["ISSUED"]
  depends_on = [
    data.external.get_rest_api_stage_arn
  ]
}
 
resource "aws_api_gateway_domain_name" "custom_domain" {
  domain_name              = var.custom_domain_name
  regional_certificate_arn = data.aws_acm_certificate.my_certificate.arn 
  security_policy          = "TLS_1_2"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
  depends_on = [
    data.aws_acm_certificate.my_certificate
  ]
}

resource "aws_api_gateway_base_path_mapping" "custom_domain_map_api" {
  api_id = aws_api_gateway_rest_api.rest_api.id
  stage_name  = var.deploy_stage_name
  domain_name = aws_api_gateway_domain_name.custom_domain.domain_name
  depends_on = [
    aws_api_gateway_domain_name.custom_domain
  ]
}

data "aws_route53_zone" "hosted_zone" {
  name         = var.hosted_zone_name
  private_zone = false
  depends_on = [
    aws_api_gateway_base_path_mapping.custom_domain_map_api
  ]
}

resource "aws_route53_record" "new_record" {
  zone_id     = data.aws_route53_zone.hosted_zone.id
  name        = var.custom_domain_name
  type        = "A"
  alias {
    name      = aws_api_gateway_domain_name.custom_domain.regional_domain_name 
    zone_id   = aws_api_gateway_domain_name.custom_domain.regional_zone_id 
    evaluate_target_health = true 
  }
  depends_on = [
    data.aws_route53_zone.hosted_zone
  ]
}
