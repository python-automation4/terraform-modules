output "rest_api_name" {
    value = aws_api_gateway_rest_api.rest_api.name
}

output "api_deploy_stage_name" {
    value = aws_api_gateway_deployment.rest_api_deployment.stage_name
}

output "rest_api_stage_arn" {
  value = data.external.get_rest_api_stage_arn.result["arn"]
}

output "target_domain_name" {
  value = aws_api_gateway_domain_name.custom_domain.regional_domain_name
}