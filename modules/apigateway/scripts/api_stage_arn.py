import subprocess
import json
import sys

region = sys.argv[1] #"ap-south-1"
api_name = sys.argv[2]

command = f'aws apigateway get-rest-apis --query "items[?name==\'{api_name}\'].id" --output text'
result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
rest_api_id = result.stdout.strip()

command = ["aws", "apigateway", "get-stages", "--rest-api-id", rest_api_id]
result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

if result.returncode == 0:
    output = result.stdout.decode('utf-8')
    stages = json.loads(output)['item']
    stage_names = [stage['stageName'] for stage in stages]
    stage_name = stage_names[0]
    arn = f'arn:aws:apigateway:{region}::/restapis/{rest_api_id}/stages/{stage_name}'
    print(json.dumps({"arn": arn}))
else:
    error_msg = result.stderr.decode('utf-8')
    print(json.dumps({"error": error_msg}))