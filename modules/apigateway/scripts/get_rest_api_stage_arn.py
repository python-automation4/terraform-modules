import subprocess
import json

region = "ap-south-1"
command = 'aws apigateway get-rest-apis --query "items[?name==\'Web_Rest_API\'].id" --output text'
result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
rest_api_id = result.stdout.strip()
#print(f"REST API ID: {rest_api_id}")

command = ["aws", "apigateway", "get-stages", "--rest-api-id", rest_api_id]
result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

if result.returncode == 0:
    output = result.stdout.decode('utf-8')
    stages = json.loads(output)['item']
    stage_names = [stage['stageName'] for stage in stages]
    #print(stage_names[0])
else:
    error_msg = result.stderr.decode('utf-8')
    print(f"Error: {error_msg}")

stage_name = stage_names[0]

arn = f'arn:aws:apigateway:{region}::/restapis/{rest_api_id}/stages/{stage_name}'
print(arn)