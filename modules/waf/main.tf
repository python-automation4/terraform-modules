# Creating an IPSet
resource "aws_wafv2_ip_set" "restapi_waf_ipset" {
  name               = var.ip_set_name
  description        = "To block a list of IP range from accessing RestAPI"
  scope              = "REGIONAL"
  ip_address_version = "IPV4"
  addresses          = var.ip_list
}

# Creating the webacls
resource "aws_wafv2_web_acl" "waf_web_acl" {
  name        = var.web_acl_name
  description = "This is used to monitor the traffic between users and API Gateway"
  scope       = "REGIONAL"
  default_action {
    allow {}
  }
  dynamic "rule" {
    for_each = var.ip_rules
    content {
        name = rule.value.name
        priority = rule.value.priority
        action {
            dynamic "count" {
                for_each = rule.value.action == "count" ? [1] : []
                content {}
            }
            
            dynamic "block" {
                for_each = rule.value.action == "block" ? [1] : []
                content {}
            }
        }
        statement {
            dynamic "ip_set_reference_statement" {
                for_each = rule.value.statement == "ip_set_reference_statement" ? [1] : []
                content {
                    arn = aws_wafv2_ip_set.restapi_waf_ipset.arn
                }
            }
            
            dynamic "rate_based_statement" {
                for_each = rule.value.statement == "rate_based_statement" ? [1] : []
                content {
                    limit              = 100
                    aggregate_key_type = "IP"
                }
            }
        }
        visibility_config {
            cloudwatch_metrics_enabled = true
            metric_name                = rule.value.name
            sampled_requests_enabled   = true
        }
    }
  }

  dynamic "rule" {
    for_each = var.managed_rules
    content {
        name = rule.value.name
        priority = rule.value.priority
        override_action {
            dynamic "none" {
                for_each = rule.value.override_action == "none" ? [1] : []
                content {}
            }

            dynamic "count" {
                for_each = rule.value.override_action == "count" ? [1] : []
                content {}
            }
        }
        statement {
            managed_rule_group_statement {
                name = rule.value.name
                vendor_name = rule.value.vendor_name
            }
        }
        visibility_config {
            cloudwatch_metrics_enabled = true
            metric_name                = rule.value.name
            sampled_requests_enabled   = true
        }
    }
  }
  visibility_config {
    cloudwatch_metrics_enabled = true
    metric_name = var.web_acl_name
    sampled_requests_enabled = true
  }
}

resource "aws_wafv2_web_acl_association" "waf_gateway_stage_association" {
    resource_arn = var.api_gateway_stage_arn
    web_acl_arn  = aws_wafv2_web_acl.waf_web_acl.arn
}