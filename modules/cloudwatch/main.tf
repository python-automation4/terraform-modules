resource "aws_cloudwatch_dashboard" "my_dashboard" {
  dashboard_name = var.dashboard_name
  
  dashboard_body = jsonencode({
    widgets = [
      {
        type    = "metric"
        width   = 12
        height  = 6
        x       = 0
        y       = 0
        properties = {
          view = "timeSeries"
          title = "API Gateway Metrics"
          region = "${var.region}"
          stacked = false
          metrics = [
            # [ "AWS/ApiGateway", "Count", "ApiName", "${aws_api_gateway_rest_api.my_api.name}", "Stage", "${var.stage}" ],
            [ "AWS/ApiGateway", "Count", "ApiName", "${var.rest_api_name}", "Stage", "${var.stage}" ],
            [ ".", "4XXError", ".", ".", ".", "." ],
            [ ".", "5XXError", ".", ".", ".", "." ],
            [ ".", "Latency", ".", ".", ".", "." ],
            [ ".", "IntegrationLatency", ".", ".", ".", "." ],
          ]
          period = 300
          stat = "Sum"
        }
      },
    ]
  })
}
