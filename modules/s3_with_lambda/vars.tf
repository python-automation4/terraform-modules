variable "s3_bucket_name" {
}

variable "s3_bucket_authfunc_key" {
}

variable "s3_bucket_policy_key" {
}

variable "s3_authfunction_local_key_path" {
}

variable "s3_policyfile_local_key_path" {
}

variable "lambda_function_name" {
}

variable "lambda-run_time" {
}

variable "okta_audience" {
}

variable "okta_issuer" {
}

variable "okta_client_id" {
}
