resource "aws_s3_bucket" "my_bucket" {
  bucket = var.s3_bucket_name
}

resource "aws_s3_object" "authfunction_file" {
  bucket = aws_s3_bucket.my_bucket.id
  key    = var.s3_bucket_authfunc_key
  source = var.s3_authfunction_local_key_path #location of the zip file on the PC
  content_type = "application/zip"
}

resource "aws_s3_object" "policy_file" {
  bucket = aws_s3_bucket.my_bucket.id
  key    = var.s3_bucket_policy_key
  source = var.s3_policyfile_local_key_path 
  content_type = "application/json"
}

resource "aws_lambda_function" "lambda_function" {
    function_name = var.lambda_function_name
    role          = aws_iam_role.lambda_role.arn #var.lambda_role_arn #role needs to have Fetch objects from S3 bucket and also creates logs
    handler       = "index_handler"
    runtime       = var.lambda-run_time
    s3_bucket     = aws_s3_bucket.my_bucket.id
    s3_key        = aws_s3_object.authfunction_file.key
    environment {
      variables = {
        OKTA_AUDIENCE    = var.okta_audience
        OKTA_ISSUER      = var.okta_issuer
        OKTA_CLIENT_ID   = var.okta_client_id
        S3_BUCKET        = aws_s3_bucket.my_bucket.id #var.s3_policy_bucket_name
        S3_OBJECT        = aws_s3_object.policy_file.key
      }
    }
}

resource "aws_iam_role" "lambda_role" {
  name = "test_lambda_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy" "lambda_role_policy" {
  name = "test_lambda_role_policy"
  role = aws_iam_role.lambda_role.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "s3:GetObject",
          "s3:ListBucket"
        ]
        Resource = [
          "${aws_s3_bucket.my_bucket.arn}",
          "${aws_s3_bucket.my_bucket.arn}/*"
        ]
      }
    ]
  })
}

